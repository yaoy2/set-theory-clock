package com.paic.arch.interviews;

/**
 * Created by loovee on 2018/3/7.
 */
public class TimeConverterImpl implements TimeConverter
{
    @Override
    public String convertTime(String aTime)
    {
        String[] times = aTime.split(":");
        int h = Integer.parseInt(times[0]);
        int m = Integer.parseInt(times[1]);
        int s = Integer.parseInt(times[2]);

        StringBuilder sb = new StringBuilder();
        //秒
        if (s % 2 == 0)
        {
            sb.append("Y").append("\r\n");
        }
        else
        {
            sb.append("O").append("\r\n");
        }

        //时
        int h5 = h / 5, h1 = h % 5;
        for (int i = 0; i < 4; i++)
        {
            if (i < h5)
            {
                sb.append("R");
            }
            else
            {
                sb.append("O");
            }
        }
        sb.append("\r\n");

        for (int i = 0; i < 4; i++)
        {
            if (i < h1)
            {
                sb.append("R");
            }
            else
            {
                sb.append("O");
            }
        }
        sb.append("\r\n");

        //分
        int m15 = m / 15, m5 = m % 15 / 5, m1 = m % 15 % 5;
        for (int i = 1; i <= 11; i++)
        {
            if (i / 3 <= m15 && (double) i / 3f <= m15)
            {
                if (i % 3 != 0)
                {
                    sb.append("Y");
                }
                else
                {
                    sb.append("R");
                }
            }
            else
            {

                if (i % 3 <= m5 && i % 3 != 0)
                {
                    sb.append("Y");
                }
                else
                {
                    sb.append("O");
                }

            }

        }
        sb.append("\r\n");

        for (int i = 0; i < 4; i++)
        {
            if (i < m1)
            {
                sb.append("Y");
            }
            else
            {
                sb.append("O");
            }
        }

        System.out.println(sb.toString());
        return sb.toString();

    }

    public static void main(String[] args)
    {
        new TimeConverterImpl().convertTime("23:59:59");
    }

}
